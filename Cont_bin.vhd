library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Cont_bin is 
	port(
		clk	: in	std_logic;
		byte	: out	std_logic_vector(7 downto 0)
	);
end Cont_bin;

architecture behavioral of Cont_bin is

	signal freq_2hz	: std_logic;
	signal Contador	: std_logic_vector(7 downto 0) := "00000000";

begin

	U0:	work.div_freq 
		generic map (fin_cont => 25_000_000) 
		port map (clk => clk, dclk => freq_2hz);

	process (freq_2hz,contador) 
	begin
		if rising_edge(freq_2hz) then
			if contador = "11111111" then
				contador <= (others => '0');
			else 
				contador <= contador + 1;
			end if;
		end if;
	end process;
	
	byte <= contador;
	
end behavioral;